#!/bin/bash

RED='\033[0;31m [dev deployment]'
GREEN='\033[0;32m [dev deployment]'
RESET='\033[0m'

# given
export OVERLAY_PATH=deployments/overlays/dev/
export REPLICA_COUNT=2
export expected_occurence_replicas=1

# do
export actual_occurence_replicas=$(kustomize build ${OVERLAY_PATH} | grep "replicas: ${REPLICA_COUNT}" | wc -l)

# assert
test $expected_occurence_replicas = $actual_occurence_replicas
if [ $? != 0 ]; then
  echo -e "${RED} number of replicas definition mismatch (expected: $expected_occurence_replicas, actual $actual_occurence_replicas) ${RESET}"
  exit 1
else
  echo -e "${GREEN} number of replicas definition ok ${RESET}"
fi
