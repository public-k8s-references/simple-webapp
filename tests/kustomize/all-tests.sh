#!/bin/bash
set -e

sh ${TEST_DIRNAME}/deployments/overlays/dev/dev-test.sh
sh ${TEST_DIRNAME}/deployments/overlays/prod/prod-test.sh
